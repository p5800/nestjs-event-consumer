export interface PostCreatedEventShape {
  id: number;
  authorId: number;
}
