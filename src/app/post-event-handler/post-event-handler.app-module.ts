import { Global, Module } from '@nestjs/common';
import { PostEventHandlerService } from '@app/post-event-handler/providers/post-event-handler.service';

/**
 * ## Module, providing handlers for post events
 */
@Global()
@Module({
  providers: [PostEventHandlerService],
  exports: [PostEventHandlerService],
})
export class PostEventHandlerAppModule {}
