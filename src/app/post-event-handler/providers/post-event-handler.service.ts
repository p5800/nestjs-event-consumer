import { Injectable } from '@nestjs/common';
import { PostCreatedEventShape } from '@app/post-event-handler/shapes/post-created-event.shape';
import { PostDeletedEventShape } from '@app/post-event-handler/shapes/post-deleted-event.shape';

@Injectable()
export class PostEventHandlerService {
  public handlePostCreated(e: PostCreatedEventShape): void {
    // Should be moved to outbound adapters
    console.log('Caught post created!');
    console.log(e);
  }

  public handlePostDeleted(e: PostDeletedEventShape): void {
    // Should be moved to outbound adapters
    console.log('Caught post deleted!');
    console.log(e);
  }
}
