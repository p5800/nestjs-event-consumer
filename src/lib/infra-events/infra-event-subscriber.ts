/**
 * ## The guy who subscribes to an events
 * ### Should be implemented at infra layer and polymorphic injected into the app
 */
export abstract class InfraEventSubscriber {
  /**
   * ## Create durable subscription for an event
   * @param topic
   */
  public abstract subscribe(topic: string): Promise<void>;
}
