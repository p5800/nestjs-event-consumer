/**
 * ## The guy who broadcasts events to infrastructure
 * ### Should be implemented at infra layer and polymorphic injected into the app
 */
export abstract class InfraEventBroadcaster {
  /**
   * ## Broadcast av event to all subscribers of corresponding topic
   * @param topic
   * @param content
   */
  public abstract broadcast(topic: string, content: unknown): Promise<void>;
}
