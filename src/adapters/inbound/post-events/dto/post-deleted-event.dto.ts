export interface PostDeletedEventDto {
  post: {
    id: number;
  };
}
