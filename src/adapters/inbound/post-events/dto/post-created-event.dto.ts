export interface PostCreatedEventDto {
  post: {
    id: number;
    title: string;
    authorId: number;
  };
}
