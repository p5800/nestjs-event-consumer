export const PostEventsTopics = {
  Created: 'post-created',
  Deleted: 'post-deleted',
};
