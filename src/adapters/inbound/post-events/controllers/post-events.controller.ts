import { Controller, OnApplicationBootstrap } from '@nestjs/common';
import { EventPattern, Payload } from '@nestjs/microservices';
import { PostEventsTopics } from '@adapters/inbound/post-events/const/post-events-topics';
import { PostEventHandlerService } from '@app/post-event-handler/providers/post-event-handler.service';
import { PostCreatedEventDto } from '@adapters/inbound/post-events/dto/post-created-event.dto';
import { PostDeletedEventDto } from '@adapters/inbound/post-events/dto/post-deleted-event.dto';
import { InfraEventSubscriber } from '@lib/infra-events/infra-event-subscriber';

@Controller()
export class PostEventsController implements OnApplicationBootstrap {
  constructor(private readonly subscriber: InfraEventSubscriber, private readonly handler: PostEventHandlerService) {}

  public async onApplicationBootstrap(): Promise<void> {
    await this.subscriber.subscribe(PostEventsTopics.Created);
    // await this.subscriber.subscribe(PostEventsTopics.Deleted);
  }

  @EventPattern(PostEventsTopics.Created)
  public onPostCreated(@Payload() data: PostCreatedEventDto) {
    this.handler.handlePostCreated({
      id: data.post.id,
      authorId: data.post.authorId,
    });
  }

  @EventPattern(PostEventsTopics.Deleted)
  public onPostDeleted(@Payload() data: PostDeletedEventDto) {
    this.handler.handlePostDeleted({
      id: data.post.id,
    });
  }
}
