import { Module } from '@nestjs/common';
import { PostEventsController } from '@adapters/inbound/post-events/controllers/post-events.controller';

/**
 * ## Module, providing inbound adapter for post events.
 * ### Requires importing
 * - InfraEventSubscriber
 */
@Module({
  controllers: [PostEventsController],
})
export class PostEventsAdapterModule {}
