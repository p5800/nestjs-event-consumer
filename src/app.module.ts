import { Module } from '@nestjs/common';
import { RabbitmqInfraModule } from '@infra/rabbitmq/rabbitmq.infra-module';
import { PostEventsAdapterModule } from '@adapters/inbound/post-events/post-events.adapter-module';
import { PostEventHandlerAppModule } from '@app/post-event-handler/post-event-handler.app-module';
import { ConfigInfraModule } from '@infra/config/config.infra-module';

@Module({
  imports: [
    /* The Core */
    // No business logic, sorry

    /* Application logic */
    PostEventHandlerAppModule,

    /* Outbound adapters */
    // Not having one :)

    /* Inbound adapters */
    PostEventsAdapterModule,

    /* Infrastructure units */
    RabbitmqInfraModule,
    ConfigInfraModule,
  ],
})
export class AppModule {}
