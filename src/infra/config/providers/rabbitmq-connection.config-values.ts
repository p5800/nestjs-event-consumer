import { RabbitmqConnectionConfig } from '@infra/rabbitmq/needs/rabbitmq-connection.config';

export class RabbitmqConnectionConfigValues extends RabbitmqConnectionConfig {
  readonly url: string = process.env.RMQ_URL ?? 'amqp://localhost:5672';
}
