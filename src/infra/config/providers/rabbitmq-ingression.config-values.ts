import { RabbitmqIngressionConfig } from '@infra/rabbitmq/needs/rabbitmq-ingression.config';

export class RabbitmqIngressionConfigValues extends RabbitmqIngressionConfig {
  readonly inboundQueue: string = 'dummy-service';
}
