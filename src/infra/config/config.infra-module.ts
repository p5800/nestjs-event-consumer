import { Global, Module } from '@nestjs/common';
import { RabbitmqConnectionConfig } from '@infra/rabbitmq/needs/rabbitmq-connection.config';
import { RabbitmqConnectionConfigValues } from '@infra/config/providers/rabbitmq-connection.config-values';
import { RabbitmqIngressionConfig } from '@infra/rabbitmq/needs/rabbitmq-ingression.config';
import { RabbitmqIngressionConfigValues } from '@infra/config/providers/rabbitmq-ingression.config-values';

@Global()
@Module({
  providers: [
    {
      provide: RabbitmqConnectionConfig,
      useClass: RabbitmqConnectionConfigValues,
    },
    {
      provide: RabbitmqIngressionConfig,
      useClass: RabbitmqIngressionConfigValues,
    },
  ],
  exports: [RabbitmqConnectionConfig, RabbitmqIngressionConfig],
})
export class ConfigInfraModule {}
