import { INestApplication, INestMicroservice, Injectable } from '@nestjs/common';
import { RabbitmqConnectionConfig } from '@infra/rabbitmq/needs/rabbitmq-connection.config';
import { RabbitmqIngressionConfig } from '@infra/rabbitmq/needs/rabbitmq-ingression.config';
import { RmqOptions, Transport } from '@nestjs/microservices';

@Injectable()
export class RabbitmqMicroservice {
  constructor(
    private readonly connectionConfig: RabbitmqConnectionConfig,
    private readonly ingressionConfig: RabbitmqIngressionConfig,
  ) {}

  public install(app: INestApplication): INestMicroservice {
    return app.connectMicroservice<RmqOptions>({
      transport: Transport.RMQ,
      options: {
        urls: [this.connectionConfig.url],
        queue: this.ingressionConfig.inboundQueue,
      },
    });
  }
}
