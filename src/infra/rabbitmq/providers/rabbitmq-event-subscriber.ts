import { InfraEventSubscriber } from '@lib/infra-events/infra-event-subscriber';
import { Inject, Injectable } from '@nestjs/common';
import { ChannelWrapperInjectionToken } from '@infra/rabbitmq/const/channel-wrapper.injection-token';
import { ChannelWrapper } from 'amqp-connection-manager';
import { RabbitmqIngressionConfig } from '@infra/rabbitmq/needs/rabbitmq-ingression.config';

@Injectable()
export class RabbitmqEventSubscriber extends InfraEventSubscriber {
  constructor(
    @Inject(ChannelWrapperInjectionToken) private readonly channel: Readonly<ChannelWrapper>,
    private readonly config: RabbitmqIngressionConfig,
  ) {
    super();
  }

  public async subscribe(topic: string): Promise<void> {
    console.log(`Subscribing for ${topic}`);
    await this.channel.bindQueue(this.config.inboundQueue, topic, '');
  }
}
