import { Global, Module } from '@nestjs/common';
import { InfraEventBroadcaster } from '@lib/infra-events/infra-event-broadcaster';
import { InfraEventSubscriber } from '@lib/infra-events/infra-event-subscriber';
import { RabbitmqEventBroadcaster } from './providers/rabbitmq-event-broadcaster';
import { ChannelWrapperInjectionToken } from './const/channel-wrapper.injection-token';
import { RabbitmqConnectionConfig } from './needs/rabbitmq-connection.config';
import amqp from 'amqp-connection-manager';
import { RabbitmqEventSubscriber } from './providers/rabbitmq-event-subscriber';
import { RabbitmqMicroservice } from '@infra/rabbitmq/providers/rabbitmq-microservice';

@Global()
@Module({
  providers: [
    {
      provide: ChannelWrapperInjectionToken,
      inject: [RabbitmqConnectionConfig],
      useFactory: (config: RabbitmqConnectionConfig) =>
        amqp.connect([config.url]).createChannel({
          name: 'Broadcaster',
          json: true,
          confirm: true,
          publishTimeout: 3000,
        }),
    },
    {
      provide: InfraEventBroadcaster,
      useClass: RabbitmqEventBroadcaster,
    },
    {
      provide: InfraEventSubscriber,
      useClass: RabbitmqEventSubscriber,
    },
    RabbitmqMicroservice,
  ],
  exports: [InfraEventSubscriber, InfraEventBroadcaster, RabbitmqMicroservice],
})
export class RabbitmqInfraModule {}
