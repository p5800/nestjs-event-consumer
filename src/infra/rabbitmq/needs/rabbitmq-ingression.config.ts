export abstract class RabbitmqIngressionConfig {
  public abstract readonly inboundQueue: string;
}
