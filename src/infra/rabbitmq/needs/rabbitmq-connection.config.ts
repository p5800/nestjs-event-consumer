export abstract class RabbitmqConnectionConfig {
  public abstract readonly url: string;
}
