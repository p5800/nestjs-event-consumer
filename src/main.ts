import { NestFactory } from '@nestjs/core';
import { AppModule } from './app.module';
import { RabbitmqMicroservice } from '@infra/rabbitmq/providers/rabbitmq-microservice';

(async () => {
  const app = await NestFactory.create(AppModule);
  await app.get(RabbitmqMicroservice).install(app).listen();
  await app.init();
})();
